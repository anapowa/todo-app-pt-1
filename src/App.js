import React, { Component } from "react";
import TodoList from "./components/todo-list/TodoList"
import Form from "./components/form/Form"
import todosList from "./todos.json";
import {v4 as uuidv4} from "uuid"

class App extends Component {
  state = {
    todos: todosList,
    // items: [], 
    // currentItem: {
    //   text: "", 
    //   key: "",
    //   submitted: false,
    //   completed: false,
  
  };

  addItem = (todo) => {
    console.log(todo)
    this.setState((state) => ({ 
      todos: [...state.todos, {...todo, 
        id: uuidv4()}]}))
  }
/* [...state.todos, {...state.todos.id, completed: !this.completed}] */ 
  handleToggle = (id) => {
    console.log("toggled", this, id )
      const newTodos = this.state.todos.map(todo => {
          // return (todo.id === id )? {...todo, completed: !todo.completed}: {...todo}
        if(todo.id === id){
          {todo.completed = !todo.completed}
        }
        return todo
      }) 
      this.setState(() => newTodos)
  }

  handleDelete = (id) => {
    const newArr = this.state.todos.filter((todo) => (todo.id !== id)
    )
    console.log(id, newArr)
    this.setState({todos: newArr})
  }

  handleDeleteCompleted = () => {
    const newArr = this.state.todos.filter((todo) => (todo.completed !== true)
    )
    this.setState({todos: newArr})
  }
  

  render() {
    // const {userId, id, title, submitted, completed} = this.state.todos
    const {todos} = this.state
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <Form addItem={this.addItem}/>
        </header>
        <TodoList todos={todos} handleToggle={this.handleToggle } 
          onDelete={this.handleDelete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button onClick={() => this.handleDeleteCompleted()} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

export default App;
