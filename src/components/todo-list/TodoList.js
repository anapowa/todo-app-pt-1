import React, { Component }from "react"
import TodoItem from "../todo-item/TodoItem"

export default class TodoList extends Component {
    render() {
      const {todos} = this.props 
      return (
        <section className="main">
          <ul className="todo-list">
          {console.table(this.props.todos)}
          
            {todos.map((todo) => (
              <TodoItem 
                key= {todo.id}
                id= {todo.id} 
                title={todo.title} 
                completed={todo.completed} 
                handleToggle={this.props.handleToggle}
                onDelete={this.props.onDelete}/>
            ))}
          </ul>
        </section>
      );
    }
  }

