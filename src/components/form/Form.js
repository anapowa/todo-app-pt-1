import React from "react"
import {v4 as uuidv4} from "uuid"

const todoFormat = {
    "userId": 1,
    "id": uuidv4(),
    "title": "",
    // "submitted": false,
    "completed": false
  }
export default class Form extends React.Component {
    state={
        ...todoFormat
    }

    handleChange = event => {
        event.persist()
        // this.setState({[event.target.name]: event.target.value})
        this.setState((state) => {
            console.log(event.target.name)
            return {
                ...state, [event.target.name]: event.target.value
            }
        })
    }
    
    handleSubmit =  (event) => {
        // this.setState({todos: [...this.state.todos, todo]})
        event.preventDefault()
        console.log(this.state)
        this.props.addItem(this.state)
        this.setState(() => (
            todoFormat
        ))
      } 
      
    // handleSubmit = event => {
    //     const {todoFormat} = this.state
    //     event.preventDefault()
    //     this.props.addItem(this.state)
    //     this.setState(() => ({
    //         todoFormat
    //     }))
    // }

    render() {
        return( <>
            <form onSubmit={this.handleSubmit}>
                <input 
                    // userId="1"
                    title= {this.state.title}
                    name="title"
                    type="text"
                    className="new-todo" 
                    placeholder="Carpe Diem" 
                    value= {this.state.title}
                    onChange={this.handleChange}
                    autoFocus 
                />
                {/* <button type="submit">Submit</button> */}
            </form></>)
    }
}
    // return <input className="new-todo" placeholder="What needs to be done?" autofocus />
