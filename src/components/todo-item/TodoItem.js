import React, { Component } from "react"

class TodoItem extends Component {
    // state = {}

    render() {
      return (
        <li key={this.props.id} className={this.props.completed ? "completed" : ""}>
          <div className="view">
            <input 
                className="toggle" type="checkbox" 
                checked={this.props.completed } 
                onChange = {(event) => {this.props.handleToggle(this.props.id, event)}}
            />
            <label>{this.props.title}</label>
            <button className="destroy" 
                onClick = {() => {this.props.onDelete(this.props.id)}}
            />
          </div>
        </li>
      );
    }
  }

  export default TodoItem